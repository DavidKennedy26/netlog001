/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/record', 'N/runtime', 'N/url', 'N/file'],
    /**
     * @param {search} search
     * @param {record} record
     * @param {runtime} runtime
     * @param {url} url
     * @param {file} file
     */
    function(search, record, runtime, url, file) {

        function beforeLoad(scriptContext) {}

        function beforeSubmit(scriptContext) {}

        function afterSubmit(scriptContext) {
            //Go and see if an expenses folder exists and if not, create it
            var folderId = createExpensesFolder(scriptContext);
            //Move attached expense files to new folder
            moveAttachedFiles(scriptContext, folderId);
        }

        /**
         * Checks to see if an expenses folder exists and if not, creates it
         */
        function createExpensesFolder(context) {
            var folderId;

            //Search and see if a folder for this expense claim exists
            var folderNameStructure = 'Expenses_Claim_' + context.newRecord.id;
            var folderSearchObj = search.create({
                type: "folder",
                filters:
                    [
                        ["name", "is" , folderNameStructure]
                    ]
            });

            folderSearchObj.run().each(function(resultfolder){
                folderId = resultfolder.id;

                log.debug('Folder found', 'Name: ' + folderNameStructure + 'ID: ' + folderId);

                return folderId;

            });

            // If we didn't find the folder, create one
            if (!folderId) {

                log.debug('No folder found', 'Creating new folder.  Folder name: ' + folderNameStructure);

                var folder = record.create({
                    type: record.Type.FOLDER
                });

                folder.setValue({fieldId: 'name', value: folderNameStructure});

                //Set parent folder
                var parentFolderId;
                var parentFolderName = "Expense Claims";
                var parentFolderIdSearch = search.create({
                    type: "folder",
                    filters:
                        [
                            ["name", "is" , parentFolderName]
                        ]
                });
                log.debug('Searching for parent folder', 'Parent folder name: ' + parentFolderName);
                parentFolderIdSearch.run().each(function(resultfolder){
                    parentFolderId = resultfolder.id;
                    log.debug('Parent folder found', 'Parent folder ID: ' + parentFolderId);
                    return folderId;
                });

                folder.setValue({fieldId: 'parent', value: parentFolderId});

                //Set permissions
                var folderPermissionGroupId = setPermissions(context, null);

                folder.setValue({fieldId: 'group', value: folderPermissionGroupId});

                // Get the Id of the Folder
                folderId = folder.save();
                log.debug('Folder saved', 'New folder ID: ' + folderId);
            }else{
                log.debug('Folder found', 'Folder ID: ' + folderId);

                //If there's a folder, update the permissions of the folder
                var folder = record.load({
                    type: 'folder',
                    id: folderId
                });

                var folderPermissionGroupId = setPermissions(context, folderId);

                folder.setValue({fieldId: 'group', value: folderPermissionGroupId});

                folder.save();
            }

            //Populate attachment folder location field with filepath to folder
            var host = url.resolveDomain({hostType: url.HostType.APPLICATION});
            var filePath = host + "/app/common/media/mediaitemfolders.nl?folder=" + folderId;

            var recordObj = record.load({type: record.Type.EXPENSE_REPORT, id: context.newRecord.id});
            recordObj.setText({
                fieldId: 'custbody_netlog001_attachment_folder',
                text: filePath
            });

            recordObj.save();

            log.debug('Expense report saved with updated filepath to attachments folder', 'Filepath: ' + filePath);

            return folderId;
        }

        /**
         * Sets up the permissions for the new file folder
         */
        function setPermissions(context, folderId) {
            //Set up the Finance Employees Saved Search and create the initial group object
            //Add the Current User and Approver to the group
            //Finally run the Saved Search and add all users in the Finance department/stream
            var groupName = 'ExpenseID-' + context.newRecord.id;

            var groupObj;
            var groupId;

            //Search to see if group already exists with naming structure above
            var expenseFolderGroupSearchResults = search.create({
                type: 'entitygroup',
                columns: [
                    'internalid',
                    'groupname'
                ],
                filters: [
                    'groupname', 'is', groupName
                ]
            });

            log.debug('Searching for expense permissions group', 'Group name: ' + groupName);

            if(expenseFolderGroupSearchResults !== null) {
                //If search returns a result then a group already exists
                expenseFolderGroupSearchResults.run().each(function(result){
                    groupId = result.id;

                    log.debug('Expense permissions group found. Deleting group before updating.', 'Group ID: ' + groupId);

                    groupObj = record.load({
                        type: 'entitygroup',
                        id: groupId
                    });

                    var folder = record.load({
                        type: 'folder',
                        id: folderId
                    });

                    folder.setValue({fieldId: 'group', value: null});

                    folder.save();

                    record.delete({type:'entitygroup', id: groupId});

                    log.debug('Expense permissions group deleted.  An updated group will now be created.');

                    return true;
                });
            }
            log.debug('Creating new expense permissions group');
            //Create new group
            groupObj = record.create({
                type: 'entitygroup',
                isDynamic: true,
                defaultValues: {
                    grouptype: 'Employee',
                    dynamic: 'F'
                }
            });

            groupObj.setValue({fieldId: 'groupname', value: groupName});
            groupObj.setValue({fieldId: 'restrictedtoowner', value: true});

            //Array to hold users already added to group, do a check to see if they've already been added before trying to add them to avoid an error
            var currentGroupMemberIds = [];

            //Add current user
            var currentUser = runtime.getCurrentUser();
            var currentUserId = currentUser.id;

            groupObj.selectNewLine({
                'sublistId': 'groupmembers'
            });

            groupObj.setCurrentSublistValue({
                'sublistId': 'groupmembers',
                'fieldId': 'employeemember',
                'value': currentUserId
            });

            currentGroupMemberIds.push(currentUserId);

            groupObj.commitLine({
                'sublistId': 'groupmembers'
            });

            log.debug('Expense permissions group -- adding current user to members', 'User ID: ' + currentUserId);

            //Add expense approver
            //Perform a search to grab current user record
            var expenseApproverSearch = search.create({
                type: search.Type.EMPLOYEE,
                columns: [
                    'entityid',
                    'approver'
                ],
                filters: [
                    'internalid', 'is', currentUserId
                ]
            });

            //If the approver field isn't empty then add it to the group permissions
            expenseApproverSearch.run().each(function(result){
                if(result.getValue('approver') != null || result.getValue('approver') != "" ){

                    //If approver is not already in the group
                    if(currentGroupMemberIds.indexOf(result.getValue('approver')) === -1){
                        //Add them to the array and the list
                        currentGroupMemberIds.push(result.getValue('approver'));

                        groupObj.selectNewLine({
                            'sublistId': 'groupmembers'
                        });

                        groupObj.setCurrentSublistValue({
                            'sublistId': 'groupmembers',
                            'fieldId': 'employeemember',
                            'value': result.getValue('approver')
                        });

                        groupObj.commitLine({
                            'sublistId': 'groupmembers'
                        });

                        log.debug('Expense permissions group -- adding expense report approver to members', 'Approver ID: ' + result.getValue('approver'));
                    }else{
                        log.debug('Expense permissions group -- expense report approver is already in the group.', 'Approver ID: ' + result.getValue('approver'));
                    }
                }
                return true;
            });

            //Add finance department
            //Get ID of finance department
            var departmentName = "Loganair Senior Finance";

            log.debug('Searching for finance department', 'Department name: ' + departmentName);

            var departmentId;
            var roleNameSearch = search.create({
                type: search.Type.ROLE,
                filters: ['name', 'is', departmentName],
                columns: ['name', 'internalid']
            });
            roleNameSearch.run().each(function(result){
                departmentId = result.getValue('internalid');
            });

            log.debug('Department found.  Will now search for employees of department.', 'Department ID: ' + departmentId);

            //Create a search to find all employees in the finance department using department ID returned from above search
            var financeEmployeeSearch = search.create({
                type: search.Type.EMPLOYEE,
                columns: [
                    'internalid',
                    'entityid'
                ],
                filters: [
                    'role', 'anyof', departmentId
                ]
            });

            //Foreach record, add them to the group
            financeEmployeeSearch.run().each(function(result){
                //If approver is not already in the group
                if(currentGroupMemberIds.indexOf(result.getValue('internalid')) === -1) {
                    //Add them to the array and the list
                    currentGroupMemberIds.push(result.getValue('internalid'));

                    groupObj.selectNewLine({
                        'sublistId': 'groupmembers'
                    });

                    groupObj.setCurrentSublistValue({
                        'sublistId': 'groupmembers',
                        'fieldId': 'employeemember',
                        'value': result.getValue('internalid')
                    });

                    groupObj.commitLine({
                        'sublistId': 'groupmembers'
                    });

                    log.debug('Employee found.  Adding to members list of permissions group.', 'Employee: ' + result.getValue('entityid'));
                }else{
                    log.debug('Employee is already in the group.', 'Employee: ' + result.getValue('entityid'));
                }

                return true;
            });

            //Save group
            var savedGroupId = groupObj.save();

            log.debug('Group successfully saved', 'Group ID: ' + savedGroupId);

            return savedGroupId;
        }

        /**
         * Move any existing/newly attached files for an expense report to the newly created folder
         */
        function moveAttachedFiles(context, targetFolderId) {

            log.debug('Moving attached files to new expense folder', 'Target folder ID: ' + targetFolderId);

            //Run a search to get expense report and return any files attached to it
            var expensereportFileSearchObj = search.create({
                type: "expensereport",
                filters:
                    [
                        ["type","anyof","ExpRept"],
                        "AND",
                        ["internalid","anyof", context.newRecord.id]
                    ],
                columns:
                    [
                        search.createColumn({
                            name: "internalid",
                            join: "file"
                        }),
                        search.createColumn({
                            name: "name",
                            join: "file"
                        }),
                        search.createColumn({
                            name: "folder",
                            join: "file"
                        })
                    ]
            });
            expensereportFileSearchObj.run().each(function(result){
                //If file's current folderId is the target folderId then no need to move it
                var resultValues = result.getAllValues();

                var fileInternalId;
                if(resultValues["file.internalid"].length > 0) {
                    fileInternalId = resultValues["file.internalid"][0]["value"];
                }

                var currentFolderId;
                if(resultValues["file.folder"].length > 0) {
                    currentFolderId = resultValues["file.internalid"][0]["value"];
                }

                if(currentFolderId != targetFolderId) {
                    try {
                        log.debug('Moving file.', 'File ID: ' + fileInternalId + '.  Target Folder ID: ' + targetFolderId);
                        var fileObj = file.load({id: fileInternalId});
                        fileObj.folder = targetFolderId;
                        fileObj.save();
                        log.debug('Successfully moved file.');
                    } catch (e) {
                        log.error('File Move Failed', 'Could not move file: ' + e.message);
                    }
                }
                return true;
            });

            return true;
        }

        return {
            //beforeLoad: beforeLoad,
            //beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        };

    });
